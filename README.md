# Deploy Firewall - Version 2.2.1

This tool installs Iptables and Nftables. It deploys too a minimalist firewall configuration.

## Requirement
* Debian 9+ or OS based on Debian 9+ (Ubuntu, Linux Mint...)
* Root permissions

## Usage
```
deploy-firewall.sh [OPTIONS]

OPTIONS:
    -h      Display help
    -i      Deploy Iptables
    -n      Deploy Nftables
    -s      Allow new and related SSH connections
```

## Behaviour

This tool has been created in order to deploy a simple but efficient firewall configuration. It's mainly useful for personal computer or simple server configuration.

Nftables is deployed if your OS is Debian 10 and more or based on this Debian release. If it's not, Iptables will be deployed. However, you can overwrite this default behaviour and force to install Nftables with -n option or Iptables with -i option.

By default, SSH connections are not allowed. If the script is launched with -s option, new and related SSH connections become allowed.

### Configuration detail

Here is Iptables rules deployed by the script :

```
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
ACCEPT     all  --  anywhere             anywhere             /* ACCEPT INPUT loopback */
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED /* ACCEPT ALL INPUT FROM RELATED AND ESTABLISHED CONNEXIONS */
ACCEPT     icmp --  anywhere             anywhere             /* ACCEPT ALL INPUT ICMP (Ping) */
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:ssh ctstate NEW,ESTABLISHED /* ACCEPT ALL INPUT SSH FROM NEW AND ESTABLISHED CONNEXIONS  - Enabled if -s option is activated */
LOG_DROP       all  --  anywhere             anywhere             /* BLOCK ALL OTHER INPUT CONNEXIONS */

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination

Chain LOG_DROP (0 references)
target     prot opt source               destination
LOG        all  --  anywhere             anywhere            /* LOG level warning prefix "[IPTABLES DROP] : " */
```

Here is Nftables rules deployed by the script :

```
table ip filter {
        chain input {
                type filter hook input priority 0; policy accept;
                iif "lo" accept comment "Allow loopback"
                ct state established,related accept comment "Allow established and related connections"
                icmp type { echo-reply, echo-request } accept comment "Allow ICMP"
                tcp dport ssh ct state established,new accept comment "Allow new and established SSH connections - Enabled if -s option is activated "
                log prefix "Nftables_drop" drop
        }
}

```

## Credits

Tool developed by Jonas Chopin-Revel on Licence GNU-AGPLv3.

Gitlab : https://framagit.org/jcr/deploy-firewall

**Share and improve it. It's free !**
