#!/bin/bash

#############
# Functions #
#############

log () {
    echo "$@"
    logger -p local6.notice -t "$SCRIPT_NAME" "$@"
}

# Manual

usage() {
cat << EOF
Deploy-firewall.sh 2.2.1

This tool installs Iptables or Nftables and deploys a simple but efficient firewall configuration. It's mainly useful for personnal computer or simple server configuration.
By default, SSH connections are not allowed. If the script is launched with -s option, new and related SSH connections are allowed.

Usage: deploy-firewall.sh [OPTIONS]

OPTIONS:
    -h      Display help
    -i      Deploy Iptables
    -n      Deploy Nftables
    -s      Allow new and related SSH connections

Tool developped by Jonas Chopin-Revel on Licence GNU-AGPLv3.
Share and improve it. It's free !
EOF
}

purge_ufw() {
    # Check if UFW is installed
    dpkg -l|grep '^ii  ufw'

    # Purge UFW
    if [ "$?" -eq 0 ]
    then
        log "UFW is installed"
        apt-get purge -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" "ufw" && apt-get autoremove -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" && log "UFW purged"
    fi
}

purge_iptables () {
    # Check if Iptables is installed
    dpkg -l|grep '^ii  iptables'

    # Purge Iptables
    if [ "$?" -eq 0 ]
    then
        log "Iptables is installed"
        apt-get purge -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" "iptables" && apt-get autoremove -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" && log "Iptables purged"
    fi
}

purge_nftables () {
    # Check if NFtables is installed
    dpkg -l|grep '^ii  nftables'

    # Purge Nftables
    if [ "$?" -eq 0 ]
    then
        log "Nftables is installed"
        DEBIAN_FRONTEND=noninteractive apt-get purge -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" "nftables" && apt-get autoremove -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" && log "Nftables purged"
    fi
}

install_iptables() {
    # Purge UFW & NFtables
    purge_ufw
    purge_nftables

    # Check if iptables is already installed
    dpkg -l|grep '^ii  iptables'

    # Install iptables
    if [ "$?" -ne 0 ]
    then
        log "Iptables is not already installed"
        DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -q -y --no-install-recommends -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" "iptables" && log "Iptables installed"
    fi

    # Check if iptables-persistent
    dpkg -l '^ii  iptables-persistent'

    # Install iptables-persistent
    if [ "$?" -ne 0 ]
    then
        log "Iptables-persistent is not installed"
        DEBIAN_FRONTEND=noninteractive apt-get install -q -y --no-install-recommends -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" iptables-persistent && log "Iptables-persistent installed"
    fi

    # Clean existent iptables rules
    iptables -F
    iptables -X

    # Add new default policy. Input default policy is on ACCEPT in order to avoid to be blocked if someone flush our new rules later.
    iptables -P INPUT ACCEPT
    iptables -P FORWARD ACCEPT
    iptables -P OUTPUT ACCEPT

    # Add news rules

    # Logging
    iptables -N LOG_DROP
    iptables -A LOG_DROP -j LOG --log-prefix '[IPTABLES DROP] : '
    iptables -A LOG_DROP -j DROP

    # Packets rules
    iptables -A INPUT -i lo -m comment --comment "ACCEPT INPUT loopback" -j ACCEPT
    iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -m comment --comment "ACCEPT ALL INPUT FROM RELATED AND ESTABLISHED CONNEXIONS" -j ACCEPT
    iptables -A INPUT -p icmp -m comment --comment "ACCEPT ALL INPUT ICMP (Ping)" -j ACCEPT
    if [ "$ENABLE_SSH" -eq 1 ]
    then
        iptables -A INPUT -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "ACCEPT ALL INPUT SSH FROM NEW AND ESTABLISHED CONNEXIONS" -j ACCEPT
        log "Allow SSH connections"
    fi
    iptables -A INPUT -m comment --comment "BLOCK ALL OTHER INPUT CONNEXIONS" -j LOG_DROP
    log "New rules deployed"
    # Saving rules
    service netfilter-persistent save
    log "Save new rules"
    EXIT_CODE=0
}

install_nftables() {
    # Check if UFW and Iptables are installed
    purge_ufw
    purge_iptables

    # Check if Nftables is already installed

    dpkg -l|grep '^ii  nftables'

    #install iptables
    if [ "$?" -ne 0 ]
    then
        log "Nftables is not already installed"
    # Install Nftables
    DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -q -y --no-install-recommends -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" "nftables"
    systemctl enable nftables
    fi
    # Flush current rules
    nft flush ruleset
    # Create table, chain and add rules
    nft add table filter
    nft add chain ip filter input { type filter hook input priority 0 \; }
    nft add rule filter input iif "lo" accept comment \"Allow loopback\"
    nft add rule filter input ct state established,related accept comment \"Allow established and related connections\"
    nft add rule filter input icmp type {echo-reply, echo-request} accept comment \"Allow ICMP\"
    if [ "$ENABLE_SSH" -eq 1 ]
    then
        nft add rule filter input tcp dport 22 ct state new,established accept comment \"Allow new and established SSH connections\"
        log "Allow SSH connections"
    fi
    nft add rule filter input log prefix \"Nftables_drop \" drop
    # Save configuration
    nft list table filter > /etc/nftables.conf
    log "Save new rules"
    EXIT_CODE=0
}

#############
# Variables #
#############

readonly SCRIPT_NAME=$(basename "$0")
BAD_OPTION=0
ENABLE_SSH=''
DEPLOY_IPTABLES=''
DEPLOY_NFTABLES=''
EXIT_CODE=''

# Retrieve parameters

while getopts "hins" OPTION
do
    case $OPTION in
        h)
            usage
            exit 0
            ;;
        i)
            DEPLOY_IPTABLES=1
            ;;
        n)
            DEPLOY_NFTABLES=1
            ;;
        s)
            ENABLE_SSH=1
            ;;
        \?)
            BAD_OPTION=1
            EXIT_CODE=1
            ;;
        \?)
            BAD_OPTION=1
            RETURN_CODE=1
            ;;
     esac
done


########
# Core #
########
log "Deploy-firewall.sh started"

if [[ $BAD_OPTION -ne 1 ]]
then
    if [ -n "$DEPLOY_IPTABLES" ] || [ -n "$DEPLOY_NFTABLES" ]
    then
        if [ "$DEPLOY_NFTABLES" == 1 ]
        then
            install_nftables
        elif [ "$DEPLOY_IPTABLES" == 1 ]
        then
            install_iptables
        fi
    else
        # Define Debian Version
        DEBIAN_VERSION=$(cat /etc/debian_version)
        if [[ "$DEBIAN_VERSION" =~ (10).* ]] || [[ "$DEBIAN_VERSION" =~ (buster).* ]] || [[ "$DEBIAN_VERSION" =~ (11).* ]] || [[ "$DEBIAN_VERSION" =~ (bullseye).* ]]
        then
            install_nftables
        else
            install_iptables
        fi
    fi
    exit $EXIT_CODE
else
    echo "### ERROR - Invalid option. ###"
    usage
    exit $EXIT_CODE
fi
